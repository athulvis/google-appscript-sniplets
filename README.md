# Google Appscript Sniplets


## [Update google drive links to googlesheet](drive_link_to_sheets.gs)

 This function can be used for updating list of files and their links in a folder inside google drive to a google sheet.

 Use it inside google sheet script editor. The list will be updated in the active cell in active sheet.

 reference: https://support.google.com/docs/thread/16362151/get-names-and-links-for-multiple-files-on-google-drive?hl=en
